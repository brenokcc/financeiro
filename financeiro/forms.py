# -*- coding: utf-8 -*-

from djangoplus.ui.components import forms
from djangoplus.decorators import action


class RelatorioForm(forms.Form):

    class Meta:
        submit_label = 'Gerar Relatório'
        title = 'Informe o Período'

    data_inicio = forms.DateField(label='Data de Início')
    data_fim = forms.DateField(label='Data de Fim')
    efetivado = forms.BooleanField(label='Apenas Efetivados', required=False, help_text='Considerar apenas as contas quitadas e seu respectivos valores pagos/recebidos.')

    def processar(self):
        return self.cleaned_data['efetivado'], self.cleaned_data['data_inicio'], self.cleaned_data['data_fim']

