# -*- coding: utf-8 -*-

from djangoplus.conf.base_settings import *
from os.path import abspath, dirname, join, exists
from os import sep

BASE_DIR = abspath(dirname(dirname(__file__)))
PROJECT_NAME = __file__.split(sep)[-2]

STATIC_ROOT = join(BASE_DIR, 'static')
MEDIA_ROOT = join(BASE_DIR, 'media')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(BASE_DIR, 'sqlite.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    },
    'OPTIONS': {
        'timeout': 20,
    }
}

WSGI_APPLICATION = '{}.wsgi.application'.format(PROJECT_NAME)

INSTALLED_APPS += (
    PROJECT_NAME,
    'endless',
)

ROOT_URLCONF = '{}.urls'.format(PROJECT_NAME)

if not exists(join(BASE_DIR, 'logs')):
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = join(BASE_DIR, 'mail')
else:
    SERVER_EMAIL = 'root@djangoplus.net'
    ADMINS = [('Admin', 'root@djangoplus.net')]
