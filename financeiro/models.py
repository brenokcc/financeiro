# -*- coding: utf-8 -*-
import datetime
from djangoplus.db import models
from djangoplus.decorators import action, subset
from django.db.models.aggregates import Sum


class Obra(models.Model):

    nome = models.CharField('Nome', null=False, blank=False, search=True)
    endereco = models.TextField('Endereço', null=True, blank=True)

    class Meta:
        verbose_name = 'Obra'
        verbose_name_plural = 'Obras'
        menu = 'Obras'
        icon = 'fa-building'

    def __str__(self):
        return self.nome


class NaturezaDespesa(models.Model):
    DEBITO = 1
    CREDITO = 2

    descricao = models.CharField('Descrição', null=False, blank=False, search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao',),}),
    )

    class Meta:
        verbose_name = 'Naturesa de Despesa'
        verbose_name_plural = 'Naturesas de Despesa'

    def __str__(self):
        return self.descricao


class Despesa(models.Model):

    natureza = models.ForeignKey(NaturezaDespesa, verbose_name='Natureza', null=False, blank=False, filter=False, exclude=True)

    class Meta:
        verbose_name = 'Tipo'
        verbose_name_plural = 'Tipos'

    def __str__(self):
        return self.descricao


class TipoReceita(Despesa):
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', 'natureza'),}),
    )

    class Meta:
        verbose_name = 'Tipos de Receita'
        verbose_name_plural = 'Tipos de Receitas'
        menu = 'Cadastros::Tipos de Receitas', 'fa-th'

    def save(self):
        natureza = NaturezaDespesa.objects.get(pk=NaturezaDespesa.CREDITO)
        self.natureza = natureza
        super(TipoReceita, self).save()


class TipoDespesa(Despesa):
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao', 'natureza'),}),
    )

    class Meta:
        verbose_name = 'Tipos de Desepsa'
        verbose_name_plural = 'Tipos de Despesas'
        menu = 'Cadastros::Tipos de Despesas'

    def save(self):
        natureza = NaturezaDespesa.objects.get(pk=NaturezaDespesa.DEBITO)
        self.natureza = natureza
        super(TipoDespesa, self).save()


class FormaPagamento(models.Model):

    descricao = models.CharField('Descrição', null=False, blank=False, search=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('descricao',),}),
    )

    class Meta:
        verbose_name = 'Forma de Pagamento'
        verbose_name_plural = 'Formas de Pagamento'
        menu = 'Cadastros::Formas de Pagamento'

    def __str__(self):
        return self.descricao


class Pessoa(models.Model):
    nome = models.CharField('Nome', null=False, blank=False, search=True)

    class Meta:
        verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoa'

    def __str__(self):
        return self.nome


class PessoaFisica(Pessoa):
    documento = models.CpfField('CPF', null=False, blank=True, search=True, default='000.000.000-00')
    telefone = models.PhoneField('Telefone', null=True, blank=True)
    endereco = models.TextField('Endereço', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', ('documento', 'telefone'), 'endereco')}),
    )

    class Meta:
        verbose_name = 'Pessoa Física'
        verbose_name_plural = 'Pessoas Físicas'
        menu = 'Pessoas::Pessoas Físicas', 'fa-users'

    def __str__(self):
        return '{} ({})'.format(self.nome, self.documento or '000.000.000-00')


class PessoaJuridica(Pessoa):
    representante = models.CharField('Representate', null=False, blank=True, search=True, default='')
    documento = models.CnpjField('CNPJ', null=False, blank=True, search=True, default='00.000.000/0000-00')
    telefone = models.PhoneField('Telefone', null=True, blank=True)
    endereco = models.TextField('Endereço', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', 'representante', ('documento', 'telefone'), 'endereco')}),
    )

    class Meta:
        verbose_name = 'Pessoa Jurídica'
        verbose_name_plural = 'Pessoas Jurídicas'
        menu = 'Pessoas::Pessoas Jurídicas', 'fa-users'
        icon = 'fa-briefcase'

    def __str__(self):
        return '{} ({})'.format(self.nome, self.documento)


class ContaReceberManager(models.Manager):

    def total(self):
        return self.all().aggregate(Sum('valor')).get('valor__sum') or 0

    @subset('Recebidas')
    def nao_pagas(self):
        return self.filter(data_pagamento__isnull=False)

    @subset('Não-Recebidas')
    def nao_pagas(self):
        return self.filter(data_pagamento__isnull=True)


class ContaReceber(models.Model):
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    tipo_receita = models.ForeignKey(TipoReceita, verbose_name='Tipo', filter=True)
    pessoa = models.ForeignKey(Pessoa, verbose_name='Cliente', null=False, blank=False, filter=True)
    obra = models.ForeignKey(Obra, verbose_name='Obra', null=True, blank=True, filter=True)
    valor = models.MoneyField('Valor (R$)', null=False, blank=False)
    forma_pagamento = models.ForeignKey(FormaPagamento, verbose_name='Forma de Pagamento', null=False, blank=False, filter=True)
    data_solicitacao = models.DateField('Data da Solicitação', null=False, blank=False, default=datetime.datetime.today)
    data_prevista_pagamento = models.DateField('Data Prevista do Pagamento', null=False, blank=False)
    data_pagamento = models.DateField('Data do Pagamento', null=True, blank=False, exclude=True)
    valor_pago = models.MoneyField('Valor (R$)', null=True, blank=False, default=0, exclude=True)
    observacao = models.TextField('Observação', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('descricao', 'tipo_receita'), ('pessoa', 'obra'), ('valor', 'forma_pagamento'), ('data_solicitacao', 'data_prevista_pagamento'))}),
        ('Dados do Pagamento', {'fields': (('data_pagamento', 'valor_pago'),)}),
        ('Outras Informações', {'fields': ('observacao',)})
    )

    class Meta:
        verbose_name = 'Conta a Receber'
        verbose_name_plural = 'Contas a Receber'
        menu = 'Contas::Contas a Receber', 'fa-usd'
        list_display = 'descricao', 'valor', 'valor_pago', 'data_prevista_pagamento', 'data_pagamento'

    objects = ContaReceberManager()

    def __str__(self):
        return self.descricao

    @action('Registrar Recebimento', condition='not data_pagamento', inline=True)
    def registrar_recebimento(self, data_pagamento, valor_pago):
        self.save()

    @action('Cancelar Recebimento', condition='data_pagamento')
    def cancelar_recebimento(self):
        self.data_pagamento = None
        self.valor_pago = 0
        self.save()


class ContaPagarManager(models.Manager):

    def total(self):
        return self.all().aggregate(Sum('valor')).get('valor__sum') or 0

    @subset('Pagas')
    def nao_pagas(self):
        return self.filter(data_pagamento__isnull=False)

    @subset('Não-Pagas')
    def nao_pagas(self):
        return self.filter(data_pagamento__isnull=True)


class ContaPagar(models.Model):
    descricao = models.CharField('Descrição', null=False, blank=False, search=True)
    tipo_despesa = models.ForeignKey(TipoDespesa, verbose_name='Tipo', filter=True)
    pessoa = models.ForeignKey(Pessoa, verbose_name='Fornecedor', null=False, blank=False, filter=True)
    obra = models.ForeignKey(Obra, verbose_name='Obra', null=True, blank=True, filter=True)
    valor = models.MoneyField('Valor (R$)', null=False, blank=False)
    forma_pagamento = models.ForeignKey(FormaPagamento, verbose_name='Forma de Pagamento', null=False, blank=False, filter=True)
    data_solicitacao = models.DateField('Data da Solicitação', null=False, blank=False, default=datetime.datetime.today)
    data_prevista_pagamento = models.DateField('Data Prevista do Pagamento', null=False, blank=False)
    data_pagamento = models.DateField('Data do Pagamento', null=True, blank=False, exclude=True)
    valor_pago = models.MoneyField('Valor (R$)', null=True, blank=False, default=0, exclude=True)
    observacao = models.TextField('Observação', null=True, blank=True)

    fieldsets = (
        ('Dados Gerais', {'fields': (('descricao', 'tipo_despesa'), ('pessoa', 'obra'), ('valor', 'forma_pagamento'), ('data_solicitacao', 'data_prevista_pagamento'))}),
        ('Dados do Pagamento', {'fields': (('data_pagamento', 'valor_pago'),), 'actions' : ('registrar_pagamento',)}),
        ('Outras Informações', {'fields': ('observacao',), })
    )

    class Meta:
        verbose_name = 'Conta a Pagar'
        verbose_name_plural = 'Contas a Pagar'
        menu = 'Contas::Contas a Pagar', 'fa-usd'
        list_display = 'descricao', 'valor', 'valor_pago', 'data_prevista_pagamento', 'data_pagamento'

    objects = ContaPagarManager()

    def __str__(self):
        return self.descricao

    @action('Registrar Pagamento', condition='not data_pagamento', inline=True)
    def registrar_pagamento(self, data_pagamento, valor_pago):
        self.save()

    @action('Cancelar Pagamento', condition='data_pagamento')
    def cancelar_pagamento(self):
        self.data_pagamento = None
        self.valor_pago = 0
        self.save()





