# -*- coding: utf-8 -*-

from djangoplus.ui.components.utils import Chart
from djangoplus.ui.components.calendar import ModelCalendar
from financeiro.models import *
from financeiro.forms import *
from djangoplus.decorators.views import view, dashboard
from django.db.models.aggregates import Sum


@dashboard()
def principal(request):
    qs_contas_pagar = ContaPagar.objects.filter(data_pagamento__isnull=True)
    qs_contas_receber = ContaReceber.objects.filter(data_pagamento__isnull=True)

    widget = ModelCalendar(request, 'Contas Pendentes', True)
    widget.add(qs_contas_pagar, 'data_prevista_pagamento', color='#fc8675', action_names=['registrar_pagamento'])
    widget.add(qs_contas_receber, 'data_prevista_pagamento', color='#65cea7', action_names=['registrar_recebimento'])

    return locals()


@view('Relatórios', menu='Relatórios', icon='fa-bar-chart-o')
def relatorios(request):
    form = RelatorioForm(request)
    if form.is_valid():
        efetivado, data_inicio, data_fim = form.processar()

        if efetivado:
            qs_contas_pagar = ContaPagar.objects.filter(data_pagamento__isnull=False, data_pagamento__gte=data_inicio, data_pagamento__lte=data_fim)
            qs_contas_receber = ContaReceber.objects.filter(data_pagamento__isnull=False, data_pagamento__gte=data_inicio, data_pagamento__lte=data_fim)
            attr = 'valor_pago'
        else:
            qs_contas_pagar = ContaPagar.objects.filter(data_prevista_pagamento__gte=data_inicio, data_prevista_pagamento__lte=data_fim)
            qs_contas_receber = ContaReceber.objects.filter(data_prevista_pagamento__gte=data_inicio, data_prevista_pagamento__lte=data_fim)
            attr = 'valor'

        tabela_contas_pagar = qs_contas_pagar.sum(attr, 'tipo_despesa', 'forma_pagamento').as_table(request)
        tabela_contas_receber = qs_contas_receber.sum(attr, 'tipo_receita', 'forma_pagamento').as_table(request)

        despesa = qs_contas_pagar.aggregate(Sum(attr)).get('{}__sum'.format(attr)) or 0
        receita = qs_contas_receber.aggregate(Sum(attr)).get('{}__sum'.format(attr)) or 0
        balancete_geral = Chart(request, labels=['Despesa', 'Receita'], series=[[float(despesa), float(receita)]], symbol='R$', title='Balancete').donut()
        tabela_despesas_por_tipo = qs_contas_pagar.sum(attr, 'tipo_despesa').as_table()
        tabela_receitas_por_tipo = qs_contas_receber.sum(attr, 'tipo_receita').as_table()
    return locals()





