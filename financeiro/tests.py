# -*- coding: utf-8 -*-

from django.conf import settings
from djangoplus.test import TestCase
from djangoplus.admin.models import User
from djangoplus.test.decorators import testcase
from django.core.management import call_command


class AppTestCase(TestCase):

    def test(self):
        call_command('loaddata', 'financeiro/fixtures/initial_data.json')
        User.objects.create_superuser(settings.DEFAULT_SUPERUSER, None, settings.DEFAULT_PASSWORD)
        self.execute_flow()

    @testcase('', username='admin')
    def fluxo_principal(self):
        # Cadastrando forma de pagamento
        self.click_menu('Cadastros', 'Formas de Pagamento')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Dinheiro')
        self.click_button('Salvar')

        # Cadastrando de tipo de despesa
        self.click_menu('Tipos de Despesas')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Pagamento de Pessoal')
        self.click_button('Salvar')

        # Cadastrando de tipo de receita
        self.click_menu('Tipos de Receitas')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Parcela de Venda')
        self.click_button('Salvar')

        # Cadastrando pessoa física
        self.click_menu('Pessoas', 'Pessoas Físicas')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Carlos Breno Pereira Silva')
        self.enter('CPF', '000.000.000-00')
        self.enter('Endereço', '')
        self.click_button('Salvar')

        # Cadastrando pessoa jurídica
        self.click_menu('Pessoas Jurídicas')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Fornecedor Padrão')
        self.enter('CNPJ', '00.000.000/0000-00')
        self.enter('Endereço', '')
        self.click_button('Salvar')

        # Cadastrando uma obra
        self.click_menu('Obras')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Parque Cidade Jardim')
        self.click_button('Salvar')

        # Cadastrando uma conta a pagar
        self.click_menu('Contas', 'Contas a Pagar')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Folha de Janeiro')
        self.choose('Tipo', 'Pagamento')
        self.choose('Fornecedor', 'Fornecedor')
        self.choose('Obra', 'Parque')
        self.enter('Valor', '1000,00')
        self.choose('Forma de Pagamento', 'Dinheiro')
        self.enter('Data da Solicitação', '01/01/2016')
        self.enter('Data Prevista do Pagamento', '01/01/2016')
        self.enter('Observação', '')
        self.click_button('Salvar')

        # registrando pagamento de conta
        self.click_menu('Contas a Pagar')
        self.look_at('Folha de Janeiro')
        self.click_icon('fa-search')
        self.click_button('Registrar Pagamento')
        self.look_at('Dados do Pagamento')
        self.enter('Data do Pagamento', '01/01/2016')
        self.enter('Valor', '1000,00', True)
        self.wait()

        # cancelando pagamento de conta
        # TODO
        self.open('/admin/')
        self.click_menu('Contas', 'Contas a Pagar')
        self.look_at('Folha de Janeiro')
        self.click_icon('fa-search')
        self.click_button('Cancelar Pagamento')

        self.logout()
